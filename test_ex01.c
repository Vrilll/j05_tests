/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   test_ex01.c                                        :+:    :+:            */
/*                                                     +:+                    */
/*   By: tjans <marvin@codam.nl>                      +#+                     */
/*                                                   +#+                      */
/*   Created: 2019/08/12 12:50:26 by tjans         #+#    #+#                 */
/*   Updated: 2019/08/13 18:29:39 by tjans         ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <limits.h>

void	ft_putnbr(int nb);

void	test(int test_int)
{
	printf("Testing %d: \n", test_int);
	ft_putnbr(test_int);
	printf("\n\n");
}
int		main(void)
{
	test(INT_MIN);
	test(INT_MAX);
	test(0);
	test(10);
	test(42);
	test(-42);
	test(1234567890);
	test(1079145107);
	return (0);
}
