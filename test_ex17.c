/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   test_ex16.c                                        :+:    :+:            */
/*                                                     +:+                    */
/*   By: tjans <marvin@codam.nl>                      +#+                     */
/*                                                   +#+                      */
/*   Created: 2019/08/13 13:11:06 by tjans         #+#    #+#                 */
/*   Updated: 2019/08/13 13:49:00 by tjans         ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include <string.h>
#include <stdio.h>

char	*ft_strncat(char *dest, char *src, int nb);

int		main(void)
{
	char	str1[12];
	char	str2[12];

	strcpy(str1, "Hello ");
	strcpy(str2, "Hello ");
	printf(	"ft_strncat: %s\nstrncat: %s\n",
			ft_strncat(str1, "World!", 5),
			strncat(str2, "World!", 5));
	return (0);
}
