/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   test_ex08.c                                        :+:    :+:            */
/*                                                     +:+                    */
/*   By: tjans <marvin@codam.nl>                      +#+                     */
/*                                                   +#+                      */
/*   Created: 2019/08/12 21:42:02 by tjans         #+#    #+#                 */
/*   Updated: 2019/08/13 20:39:53 by tjans         ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>

char	*ft_strupcase(char *str);

int		main(void)
{
	char test[] = "test string.";

	printf("%s\n", ft_strupcase(test));
	return (0);
}
