/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   test_ex05.c                                        :+:    :+:            */
/*                                                     +:+                    */
/*   By: tjans <marvin@codam.nl>                      +#+                     */
/*                                                   +#+                      */
/*   Created: 2019/08/12 19:28:11 by tjans         #+#    #+#                 */
/*   Updated: 2019/08/13 19:53:40 by tjans         ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include <string.h>
#include <stdio.h>

char	*ft_strstr(char *str, char *to_find);

void	test(char *str, char *to_find)
{
	char *result_ptr;
	char *result2_ptr;

	printf("Test string: %s\nNeedle: %s\n", str, to_find);
	result_ptr = ft_strstr(str, to_find);
	result2_ptr = strstr(str, to_find);
	printf("Result ft_strstr: %p\nResult strstr: %p\n---\n", result_ptr, result2_ptr);
}

int		main(void)
{
	char	*test1;
	
	test1 = "Bedankt voor je bestelling. Help onze service te verbeteren en vul onderstaande vragen in! Zodra je bestelling klaar is, worden alle vragen beschikbaar.";
	test(test1, "je");
	test(test1, ",");
	test(test1, "vul onderstaande vragen in!");
	test(test1, "verbeteren");
	test(test1, "NiEt ViNdBaAr");
	return (0);
}
