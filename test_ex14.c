/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   test_ex11.c                                        :+:    :+:            */
/*                                                     +:+                    */
/*   By: tjans <marvin@codam.nl>                      +#+                     */
/*                                                   +#+                      */
/*   Created: 2019/08/13 12:26:46 by tjans         #+#    #+#                 */
/*   Updated: 2019/08/13 12:55:21 by tjans         ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>

int		ft_str_is_uppercase(char *str);

void	test(char *str, int expected_output)
{
	printf("Testing string: %s\n Expected result: %s\n\033",
			str,
			(expected_output) ? "true" : "false");
	if (ft_str_is_uppercase(str) == expected_output)
		printf("[1;32mTEST PASSED");
	else
		printf("[1;31mTEST FAILED");
	printf("\033[0m\n");
}

int		main(void)
{
	test("0123456789", 0);
	test("ONLYUP", 1);
	test("theresUppercase", 0);
	return (0);
}
