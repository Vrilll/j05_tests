/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   test_ex11.c                                        :+:    :+:            */
/*                                                     +:+                    */
/*   By: tjans <marvin@codam.nl>                      +#+                     */
/*                                                   +#+                      */
/*   Created: 2019/08/13 12:26:46 by tjans         #+#    #+#                 */
/*   Updated: 2019/08/13 12:42:32 by tjans         ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>

int		ft_str_is_alpha(char *str);

void	test(char *str, int expected_output)
{
	printf("Testing string: %s\n Expected result: %s\n\033",
			str,
			(expected_output) ? "true" : "false");
	if (ft_str_is_alpha(str) == expected_output)
		printf("[1;32mTEST PASSED");
	else
		printf("[1;31mTEST FAILED");
	printf("\033[0m\n");
}

int		main(void)
{
	test("invalid!string", 0);
	test("onlyalpha", 1);
	test("no whitespace pls", 0);
	return (0);
}
