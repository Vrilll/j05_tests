/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   test_ex08.c                                        :+:    :+:            */
/*                                                     +:+                    */
/*   By: tjans <marvin@codam.nl>                      +#+                     */
/*                                                   +#+                      */
/*   Created: 2019/08/12 21:42:02 by tjans         #+#    #+#                 */
/*   Updated: 2019/08/13 20:46:59 by tjans         ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>

char	*ft_strcapitalize(char *str);

int		main(void)
{
	char test[] = "tHe friCKIng foX shoult coUnt the#!)SLEEPN be!auty";
	printf("%s\n", ft_strcapitalize(test));
	return (0);
}
