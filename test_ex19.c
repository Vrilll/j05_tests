/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   test_ex19.c                                        :+:    :+:            */
/*                                                     +:+                    */
/*   By: tjans <marvin@codam.nl>                      +#+                     */
/*                                                   +#+                      */
/*   Created: 2019/08/13 13:57:01 by tjans         #+#    #+#                 */
/*   Updated: 2019/08/13 14:31:57 by tjans         ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <string.h>

unsigned int	ft_strlcpy(char *dest, char *src, unsigned int len);

int				main(void)
{
	char				dest[16];
	unsigned int 		len;
	unsigned long		len2;

	len = ft_strlcpy(dest, "Hello U FAILED", 6);
	len2 = strlcpy(dest, "Hello U FAILED", 6);
	printf("len: %u\nlen2: %lu\n", len, len2);
	return (0);
}
