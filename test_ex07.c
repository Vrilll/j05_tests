/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   test_ex06.c                                        :+:    :+:            */
/*                                                     +:+                    */
/*   By: tjans <marvin@codam.nl>                      +#+                     */
/*                                                   +#+                      */
/*   Created: 2019/08/12 21:14:13 by tjans         #+#    #+#                 */
/*   Updated: 2019/08/12 21:40:34 by tjans         ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <string.h>

int	ft_strncmp(char *s1, char *s2, unsigned int n);

int	main(void)
{
	int t1, t2, t3;

	t2 = 0;
	t3 = 0;
	t1 = ft_strncmp("This is equal.", "This is not equal.", 4);
	printf("t1: %d, t2: %d, t3: %d\n", t1, t2, t3);
	t1 = strncmp("This is equal.", "This is not equal.", 4);
	printf("t1: %d, t2: %d, t3: %d\n", t1, t2, t3);
	return (0);
}
