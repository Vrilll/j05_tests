/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   test_ex06.c                                        :+:    :+:            */
/*                                                     +:+                    */
/*   By: tjans <marvin@codam.nl>                      +#+                     */
/*                                                   +#+                      */
/*   Created: 2019/08/12 21:14:13 by tjans         #+#    #+#                 */
/*   Updated: 2019/08/12 21:22:16 by tjans         ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <string.h>

int	ft_strcmp(char *s1, char *s2);

int	main(void)
{
	int t1, t2, t3;

	t1 = ft_strcmp("This is equal.", "This is not equal.");
	t2 = ft_strcmp("This is not equal.", "This is equal.");
	t3 = ft_strcmp("The red fox", "Die rote Fuchs");
	printf("t1: %d, t2: %d, t3: %d\n", t1, t2, t3);
	t1 = strcmp("This is equal.", "This is not equal.");
	t2 = strcmp("This is not equal.", "This is equal.");
	t3 = strcmp("The red fox", "Die rote Fuchs");
	printf("t1: %d, t2: %d, t3: %d\n", t1, t2, t3);
	return (0);
}
