/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   test_ex03.c                                        :+:    :+:            */
/*                                                     +:+                    */
/*   By: tjans <marvin@codam.nl>                      +#+                     */
/*                                                   +#+                      */
/*   Created: 2019/08/12 18:54:52 by tjans         #+#    #+#                 */
/*   Updated: 2019/08/12 19:07:01 by tjans         ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>

char	*ft_strcpy(char *dest, char *src);

int		main(void)
{
	char	dest_string[16];
	char	*strcpy_returnvalue;

	strcpy_returnvalue = ft_strcpy(dest_string, "Hello world");
	printf("Original string: Hello world\nCopied string using ft_strcpy: %s\n", dest_string);
	return (0);
}
