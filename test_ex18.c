/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   test_ex16.c                                        :+:    :+:            */
/*                                                     +:+                    */
/*   By: tjans <marvin@codam.nl>                      +#+                     */
/*                                                   +#+                      */
/*   Created: 2019/08/13 13:11:06 by tjans         #+#    #+#                 */
/*   Updated: 2019/08/13 14:29:10 by tjans         ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include <string.h>
#include <stdio.h>

unsigned int	ft_strlcat(char *dest, char *src, int nb);

int				main(void)
{
	char	str1[12];
	char	str2[12];

	strcpy(str1, "Hello ");
	strcpy(str2, "Hello ");
	printf(	"ft_strlcat: %u\nstrlcat: %lu\n",
			ft_strlcat(str1, "World!", 5),
			strlcat(str2, "World!", 5));
	return (0);
}
