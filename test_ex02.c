/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   test_ex02.c                                        :+:    :+:            */
/*                                                     +:+                    */
/*   By: tjans <marvin@codam.nl>                      +#+                     */
/*                                                   +#+                      */
/*   Created: 2019/08/12 13:09:17 by tjans         #+#    #+#                 */
/*   Updated: 2019/08/13 17:55:36 by tjans         ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <stdio.h>
#include <limits.h>

int		ft_atoi(char *str);

void	test(char *num)
{
	printf("%s:\nft_atoi: %d\natoi: %d\n", num, ft_atoi(num), atoi(num));
	printf((ft_atoi(num) == atoi(num)) ? "\033[1;32mOK\033[0m" : "\033[1;31mFAIL\033[0m");
	printf("\n---\n");
}

int		main(void)
{
	test("42");
	test("-42");
	test("-9223372036854775807");
	test("9223372036854775807");
	test("42e1");
	test("e");
	test("+10");
	test("-");
	test("--6@!$nv ,.,t");
	test("ersighi+-2nser\n\nrewfuha");
	test("123456789");
	test(" 42whitespace");
	test("  -42whitespace");
	test("- +12");
	test("+-12");
	test("-12-33");
	test("-12+33");
}
