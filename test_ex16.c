/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   test_ex16.c                                        :+:    :+:            */
/*                                                     +:+                    */
/*   By: tjans <marvin@codam.nl>                      +#+                     */
/*                                                   +#+                      */
/*   Created: 2019/08/13 13:11:06 by tjans         #+#    #+#                 */
/*   Updated: 2019/08/13 13:36:21 by tjans         ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include <string.h>
#include <stdio.h>

char	*ft_strcat(char *dest, char *src);

int		main(void)
{
	char	str1[12];
	char	str2[12];

	strcpy(str1, "Hello ");
	strcpy(str2, "Hello ");
	printf(	"ft_strcat: %s\nstrcat: %s\n",
			ft_strcat(str1, "World"),
			strcat(str2, "World"));
	return (0);
}
