/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   test_ex04.c                                        :+:    :+:            */
/*                                                     +:+                    */
/*   By: tjans <marvin@codam.nl>                      +#+                     */
/*                                                   +#+                      */
/*   Created: 2019/08/12 19:08:15 by tjans         #+#    #+#                 */
/*   Updated: 2019/08/13 19:53:03 by tjans         ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <string.h>

char	*ft_strncpy(char *dest, char *src, unsigned int len);

int		main(void)
{
	char	dest[5];

	ft_strncpy(dest, "12345", 4);
	printf("ft_strncpy(4 max): %s\n", dest);
	strncpy(dest, "12345", 4);
	printf("strncpy(4 max): %s\n", dest);
	return (0);
}
