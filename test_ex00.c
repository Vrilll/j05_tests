/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   test_ex00.c                                        :+:    :+:            */
/*                                                     +:+                    */
/*   By: tjans <marvin@codam.nl>                      +#+                     */
/*                                                   +#+                      */
/*   Created: 2019/08/12 12:36:11 by tjans         #+#    #+#                 */
/*   Updated: 2019/08/12 12:41:41 by tjans         ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

void	ft_putstr(char *str);

int		main(void)
{
	ft_putstr("Hello world!\nThis test passed successfully!\n");
	return (0);
}
