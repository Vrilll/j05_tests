/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   test_ex08.c                                        :+:    :+:            */
/*                                                     +:+                    */
/*   By: tjans <marvin@codam.nl>                      +#+                     */
/*                                                   +#+                      */
/*   Created: 2019/08/12 21:42:02 by tjans         #+#    #+#                 */
/*   Updated: 2019/08/14 13:02:42 by tjans         ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>

char	*ft_strlowcase(char *str);

int		main(void)
{
	char test[] = "this should already be lowercase! AND THIS WILL BE :)";
	printf("%s\n", ft_strlowcase(test));
	return (0);
}
